// link to app
import App from './app';
// config variables
import './utils/config';

const app = new App();

app.listen();
