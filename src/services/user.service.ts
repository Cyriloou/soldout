import { Request, Response } from 'express';
import { MongooseDocument } from 'mongoose';
import UserModel from '../mongo/typegooseModels/user';

export class UserService {
  public welcomeMessage(req: Request, res: Response) {
    return res.status(200).send('Welcome to pokeAPI REST by Nya ^^');
  }

  // Getting data from the db
  public getAllUsers(req: Request, res: Response) {
    UserModel.find({}, (error: Error, user: MongooseDocument) => {
      if (error) {
        res.send(error);
      }
      res.json(user);
    });
  }
}
