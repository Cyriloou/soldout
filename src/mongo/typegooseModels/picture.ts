import { prop, modelOptions, getModelForClass, Ref } from '@typegoose/typegoose';
import { Schema } from 'mongoose';
import { Status } from '../enums';
import { isUrl } from '../validation';
import { Sneaker } from './sneaker';
import { User } from './user';
import { Base } from './_base';

@modelOptions({
  schemaOptions: {
    collection: 'Pictures',
  },
})
export class Picture extends Base {
  @prop({ ref: 'User', refType: Schema.Types.ObjectId })
  public authorUID!: Ref<User>;

  @prop({ ref: 'Sneaker', refType: Schema.Types.ObjectId })
  public sneakerUID?: Ref<Sneaker>;

  @prop({ trim: true, lowercase: true, minLength: 0, maxLength: 12, required: true })
  public title!: string;

  @prop({ trim: true, lowercase: true, minLength: 0, maxLength: 12, required: true })
  public alt?: string;

  @prop({ min: 0, required: true, default: 0 })
  public viewCount!: string;

  @prop({ min: 0, required: true, default: 0 })
  public score!: string;

  @prop({
    trim: true,
    minLength: 0,
    required: true,
    validate: { validator: isUrl, message: '{VALUE} is not a valid url' },
  })
  public url!: string;

  @prop({ required: true, default: false })
  public hidden!: string;

  @prop({ required: true, enum: Status, default: Status.Approved })
  public status!: Status;
}

const PictureModel = getModelForClass(Picture);
export default PictureModel;
