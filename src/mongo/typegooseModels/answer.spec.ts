import AnswerModel, { Answer } from './answer';
import SneakerModel, { Sneaker } from './sneaker';
import UserModel, { User } from './user';
import { setupDB } from '../mongo-test-setup';
import _ from 'lodash';
import { sneakerDataMini } from './sneaker.spec';
import { userDataMini } from './user.spec';
import { mongoose } from '@typegoose/typegoose';
const answerDataMini: object = { body: 'here is my answer' };

// Setup a Test Database
setupDB();

describe('Test answer model', () => {
  it('Bdd empty', async () => {
    const count = await AnswerModel.find({}).estimatedDocumentCount();
    expect(count).toEqual(0);
  });

  describe('create answer', () => {
    it('create answer', async done => {
      const user = await UserModel.create(userDataMini as User);
      const sneaker = await SneakerModel.create(sneakerDataMini as Sneaker);
      const validAnswer = new AnswerModel({
        ...answerDataMini,
        authorUID: user._id,
        authorDisplayName: user.firstName,
        sneakerUID: sneaker._id,
      } as Answer);
      const savedAnswer = await validAnswer.save();
      expect(savedAnswer._id).toBeDefined();
      done();
    });

    it('create answer without required field should failed', async () => {
      const answerWithoutRequiredField = new AnswerModel(_.omit(answerDataMini, ['body']) as Answer);
      let err;
      try {
        const savedAnswerWithoutRequiredField = await answerWithoutRequiredField.save();
        err = savedAnswerWithoutRequiredField;
      } catch (error) {
        err = error;
      }
      expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
      expect(err.errors.body).toBeDefined();
    });
  });
});
