import { prop, getModelForClass, DocumentType, modelOptions, pre } from '@typegoose/typegoose';
import { Base } from './_base';

export class Address extends Base {
  @prop({ trim: true, lowercase: true, minLength: 0, maxLength: 12 })
  public streetNumber?: string;

  @prop({ trim: true, lowercase: true, minLength: 0, maxLength: 12, required: true })
  public line1!: string;

  @prop({ trim: true, lowercase: true, minLength: 0, maxLength: 12 })
  public line2?: string;

  @prop({ required: true, trim: true, lowercase: true, minLength: 5, maxLength: 12 })
  public postalCode!: string;

  @prop({ required: true, trim: true, lowercase: true, minLength: 3, maxLength: 12 })
  public city!: string;

  @prop({ required: true, trim: true, lowercase: true, minLength: 6, maxLength: 12, default: 'France' })
  public country!: string;

  // constructor(streetNumber?: string, line1: string, line2?: string, postalCode: string, city: string, country: string) {
  //   super();
  //   this.streetNumber = streetNumber;
  //   this.line1 = line1;
  //   this.line2 = line2;
  //   this.postalCode = postalCode;
  //   this.city = city;
  //   this.country = country;
  // }

  // this will create a virtual property called 'fullAddress'
  public get fullAddress(this: DocumentType<Address>) {
    return `${this.streetNumber} ${this.line1}<\br> ${this.line2} <\br>${this.postalCode} ${this.city} <\br>${this.country}`;
  }
}

export const AddressModel = getModelForClass(Address);
