import { AddressModel, Address } from './address';
import { setupDB } from '../mongo-test-setup';
import mongoose from 'mongoose';
import _ from 'lodash';
const addressDataMini = { streetNumber: '1', line1: 'Avenue des Champs-Élysées', postalCode: '75008', city: 'Paris' };
// Setup a Test Database
setupDB();

describe('Test address model', () => {
  it('Bdd empty', async () => {
    const count = await AddressModel.find({}).estimatedDocumentCount();
    expect(count).toEqual(0);
  });

  describe('create address', () => {
    it('create address', async done => {
      const validAddress = new AddressModel(addressDataMini as Address);
      const savedAddress = await validAddress.save();
      expect(savedAddress._id).toBeDefined();
      expect(savedAddress.streetNumber).toBeTruthy();
      expect(savedAddress.updatedAt).toBeTruthy();
      expect(savedAddress.line1).toBe(addressDataMini.line1.toLowerCase());
      expect(savedAddress.postalCode).toBe(addressDataMini.postalCode);
      expect(savedAddress.city).toBeTruthy();
      done();
    });

    it('create address without required field should failed', async () => {
      const addressWithoutRequiredField = new AddressModel(_.omit(addressDataMini, ['postalCode', 'line1']));
      let err;
      try {
        const savedAddressWithoutRequiredField = await addressWithoutRequiredField.save();
        err = savedAddressWithoutRequiredField;
      } catch (error) {
        err = error;
      }
      expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
      expect(err.errors.postalCode).toBeDefined();
      expect(err.errors.line1).toBeDefined();
    });
  });
});
