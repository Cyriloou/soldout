import { prop, modelOptions, getModelForClass, Ref } from '@typegoose/typegoose';
import { Schema } from 'mongoose';
import { Status, RaffleType } from '../enums';
import { isUrl } from '../validation';
import { User } from './user';
import { Sneaker } from './sneaker';
import { Base } from './_base';

@modelOptions({
  schemaOptions: {
    collection: 'Raffles',
  },
})
export class Raffle extends Base {
  @prop({ required: true, enum: RaffleType, default: RaffleType.raffle })
  public type!: RaffleType;

  @prop({ ref: 'Sneaker', refType: Schema.Types.ObjectId })
  public sneakerUID!: Ref<Sneaker>;

  @prop({ ref: 'User', refType: Schema.Types.ObjectId })
  public authorUID!: Ref<User>;

  @prop({
    required: true,
    trim: true,
    minLength: 0,
    validate: { validator: isUrl, message: '{VALUE} is not a valid url' },
  })
  public url!: string;

  @prop({ required: true, trim: true, lowercase: true, minLength: 0, maxLength: 12 })
  public website!: string;

  @prop({ required: true, default: 0, min: 0 })
  public upVoteCount!: number;

  @prop({ required: true, default: 0, min: 0 })
  public downVoteCount!: number;

  // this will create a virtual property called 'fullAddress'
  public get score() {
    return this.upVoteCount - this.downVoteCount;
  }

  @prop({ required: true, default: false })
  public hidden!: string;

  @prop({ required: true, enum: Status, default: Status.Approved })
  public status!: Status;
}

const RaffleModel = getModelForClass(Raffle);
export default RaffleModel;
