import { prop, modelOptions, getModelForClass, Ref, arrayProp } from '@typegoose/typegoose';
import { Schema } from 'mongoose';
import { Status, ProductCategory, SneakerGender } from '../enums';
import { isUrl } from '../validation';
import { Raffle } from './raffle';
import { Picture } from './picture';
import { User } from './user';
import { Base } from './_base';

@modelOptions({
  schemaOptions: {
    collection: 'Sneakers',
  },
})
export class Sneaker extends Base {
  @prop({ required: true, enum: ProductCategory, default: ProductCategory.sneaker })
  public productCategory!: ProductCategory;

  @arrayProp({ ref: 'Raffle', refType: Schema.Types.ObjectId })
  public raffles?: Raffle[];

  @arrayProp({ ref: 'Raffle', refType: Schema.Types.ObjectId })
  public sortiesMagasin?: Raffle[];

  @arrayProp({ items: String, innerOptions: { trim: true, lowercase: true, minLength: 0, maxLength: 20 } })
  public tags?: string[];

  @prop({ required: true, enum: SneakerGender, default: SneakerGender.unisex })
  public gender!: SneakerGender;

  @prop({ ref: 'Picture', refType: Schema.Types.ObjectId })
  public mainPicture?: Ref<Picture>;

  @arrayProp({ ref: 'Picture', refType: Schema.Types.ObjectId })
  public caroussel?: Picture[];

  @prop({
    required: true,
    trim: true,
    lowercase: true,
    minLength: 0,
    maxLength: 12,
  })
  public brand!: string;

  @prop({
    required: true,
    trim: true,
    lowercase: true,
    minLength: 0,
    maxLength: 24,
  })
  public modelName!: string;

  @prop({
    trim: true,
    minLength: 0,
    maxlength: 500,
  })
  public description!: string;

  @prop({
    trim: true,
    minLength: 0,
  })
  public fullDesc!: string;

  @prop({
    trim: true,
    lowercase: true,
    minLength: 0,
    maxLength: 12,
  })
  public brandSerial?: string;

  @prop({
    minLength: 0,
    maxLength: 24,
  })
  public sku?: string;

  @prop({
    minLength: 0,
    maxLength: 24,
  })
  public mpn?: string;

  @prop({
    trim: true,
    lowercase: true,
    minLength: 0,
    maxLength: 24,
  })
  public colour?: string;

  @prop({
    default: 0,
    min: 0,
    max: 3000,
  })
  public price?: number;

  @prop({
    required: true,
    default: 10,
    min: 0,
    max: 3000,
  })
  public priceMarkUp!: number;

  @prop({
    required: true,
    default: 0,
    min: -1000,
    max: 3000,
  })
  public estimatePrice!: number;

  @prop({
    required: true,
    default: 0,
  })
  public estimatePriceMin!: number;

  @prop({
    required: true,
    default: 1000,
  })
  public estimatePriceMax!: number;

  @prop({ required: true, default: 0, min: 0 })
  public profit!: number;

  @prop({ required: true })
  public launchDate!: Date;

  @prop({
    trim: true,
    minLength: 0,
    validate: { validator: isUrl, message: '{VALUE} is not a valid url' },
  })
  public stockX360Pict?: string;

  @prop({ required: true, default: 0, min: 0 })
  public viewCount!: number;

  @prop({ required: true, default: false })
  public hidden!: string;

  @prop({ required: true, enum: Status, default: Status.Approved })
  public status!: Status;

  @prop({ ref: 'User', refType: Schema.Types.ObjectId })
  public authorUID!: Ref<User>;

  // this will create a virtual property called 'fullAddress'
  public get title() {
    return `${this.brand} - ${this.modelName}`;
  }
}

const SneakerModel = getModelForClass(Sneaker);
export default SneakerModel;
