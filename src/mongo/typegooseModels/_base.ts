import { prop, modelOptions } from '@typegoose/typegoose';
import { Types } from 'mongoose';

@modelOptions({ schemaOptions: { timestamps: true } })
export abstract class Base {
  public createdAt?: Date;
  public updatedAt?: Date;
  // tslint:disable:variable-name
  public _id?: Types.ObjectId;
  public __v?: number;
  //   @prop()
  //   public __t?: undefined | string | number;
}
