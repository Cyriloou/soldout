import { prop, modelOptions, getModelForClass, Ref } from '@typegoose/typegoose';
import { Schema } from 'mongoose';
import { Status, QuestionType } from '../enums';
import { Sneaker } from './sneaker';
import { User } from './user';
import { Question } from './question';
import { Base } from './_base';

export class Answer extends Base {
  @prop({ required: true, enum: QuestionType, default: QuestionType.Answer })
  public type!: QuestionType;

  @prop({ ref: 'Question', refType: Schema.Types.ObjectId })
  public ParentId?: Ref<Question>;

  @prop({ ref: 'Sneaker', refType: Schema.Types.ObjectId })
  public sneakerUID!: Ref<Sneaker>;

  @prop({ ref: 'User', refType: Schema.Types.ObjectId })
  public authorUID!: Ref<User>;

  @prop({ required: true, trim: true, lowercase: true, minLength: 0, maxLength: 12 })
  public authorDisplayName!: string;

  @prop({ required: true, default: 0, min: 0 })
  public viewCount!: number;

  // @prop({ required: true, default: 0 })
  // public score!: string;

  @prop({ required: true, default: 0, min: 0 })
  public upVoteCount!: number;

  @prop({ required: true, default: 0, min: 0 })
  public downVoteCount!: number;

  @prop({ required: true, trim: true, lowercase: true, minLength: 0, maxLength: 500 })
  public body!: string;

  @prop({ required: true, default: false })
  public hidden!: string;

  @prop({ required: true, enum: Status, default: Status.Approved })
  public status!: Status;

  // this will create a virtual property called 'fullAddress'
  public get score() {
    return this.upVoteCount - this.downVoteCount;
  }
}

const AnswerModel = getModelForClass(Answer);
export default AnswerModel;
