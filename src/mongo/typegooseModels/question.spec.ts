import QuestionModel, { Question } from './question';
import { setupDB } from '../mongo-test-setup';
import mongoose from 'mongoose';
import _ from 'lodash';
import UserModel, { User } from './user';
import { userDataMini } from './user.spec';
import { sneakerDataMini } from './sneaker.spec';
import SneakerModel, { Sneaker } from './sneaker';

const questionDataMini: object = { body: 'here is my question', title: 'Question 1' };

// Setup a Test Database
setupDB();

describe('Test question model', () => {
  it('Bdd empty', async () => {
    const count = await QuestionModel.find({}).estimatedDocumentCount();
    expect(count).toEqual(0);
  });

  describe('create question', () => {
    it('create question', async done => {
      const user = await UserModel.create(userDataMini as User);
      const sneaker = await SneakerModel.create(sneakerDataMini as Sneaker);
      const validQuestion = new QuestionModel({
        ...questionDataMini,
        authorUID: user._id,
        authorDisplayName: user.firstName,
        sneakerUID: sneaker._id,
      });
      const savedQuestion = await validQuestion.save();
      expect(savedQuestion._id).toBeDefined();
      done();
    });

    it('create question without required field should failed', async () => {
      const questionWithoutRequiredField = new QuestionModel(_.omit(questionDataMini, ['body', 'title']));
      let err;
      try {
        const savedQuestionWithoutRequiredField = await questionWithoutRequiredField.save();
        err = savedQuestionWithoutRequiredField;
      } catch (error) {
        err = error;
      }
      expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
      expect(err.errors.authorDisplayName).toBeDefined();
      expect(err.errors.body).toBeDefined();
    });
  });
});
