import UserModel, { User } from './user';
import { setupDB } from '../mongo-test-setup';
import mongoose from 'mongoose';

export const userDataMini = { firstName: 'John', password: 'test', email: 'test@free.fr' };
// Setup a Test Database
setupDB();

describe('Test user model', () => {
  it('Bdd empty', async () => {
    const count = await UserModel.find({}).estimatedDocumentCount();
    expect(count).toEqual(0);
  });

  describe('create user', () => {
    it('create user', async done => {
      const validUser = new UserModel(userDataMini);
      const savedUser = await validUser.save();
      expect(savedUser._id).toBeDefined();
      expect(savedUser!.firstName).toBeTruthy();
      expect(savedUser!.createdAt).toBeTruthy();
      expect(savedUser.firstName).toBe(userDataMini.firstName.toLowerCase());
      expect(savedUser.reputation).toBe(0);
      expect(savedUser.password).toBeUndefined();
      expect(savedUser.createdAt).toBeDefined();
      expect(savedUser.fullName).toBe(savedUser.lastName ? `${savedUser.firstName} ${savedUser.lastName}` : savedUser.firstName);
      done();
    });

    it('create user with email already registered', async done => {
      const user1 = await UserModel.create(userDataMini as User);
      const user2 = new UserModel(userDataMini);
      let err;
      try {
        const savedUserWithoutRequiredField = await user2.save();
        err = savedUserWithoutRequiredField;
      } catch (error) {
        err = error;
      }
      expect(err.code).toBe(11000);
      expect(err.name).toBe('MongoError');
      done();
    });

    it('create user without required field should failed', async () => {
      const userWithoutRequiredField = new UserModel({ lastName: 'TekLoon' });
      let err;
      try {
        const savedUserWithoutRequiredField = await userWithoutRequiredField.save();
        err = savedUserWithoutRequiredField;
      } catch (error) {
        err = error;
      }
      expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
      expect(err.errors.email).toBeDefined();
      expect(err.errors.firstName).toBeDefined();
    });
  });
});
