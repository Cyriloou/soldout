import { prop, DocumentType, getModelForClass, Ref, arrayProp, mapProp, modelOptions } from '@typegoose/typegoose';
import { Schema } from 'mongoose';
import { Roles, Genders } from '../enums';
import { isEmail } from '../validation';
import { Vote } from './vote';
import { Address } from 'cluster';
import { Base } from './_base';

@modelOptions({
  schemaOptions: {
    collection: 'Users',
  },
})
export class User extends Base {
  @prop({
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
    validate: { validator: value => isEmail(value), message: '{VALUE} is not a valid email' },
  })
  public email!: string;

  @prop({
    unique: true,
    trim: true,
    set: (val: string) => val,
    get: (val: string) => undefined,
  })
  public password?: string;

  @prop({ required: true, trim: true, lowercase: true, minLength: 6, maxLength: 12 })
  public firstName!: string;

  @prop({ trim: true, lowercase: true, minLength: 6, maxLength: 12 })
  public lastName?: string;

  @prop({ trim: true, lowercase: true, minLength: 3, maxLength: 12 })
  public username?: string;

  @prop({ enum: Genders })
  public gender?: Genders;

  @prop({ required: true, enum: Roles, default: Roles.Guest })
  public role!: Roles;

  @arrayProp({ ref: 'Vote', refType: Schema.Types.ObjectId })
  public votes?: Vote[];

  @mapProp({ of: String })
  public googleAddress!: Map<string, string>;

  @arrayProp({ ref: 'Address', refType: Schema.Types.ObjectId })
  public addresses?: Array<Ref<Address>>;

  @prop({ required: true, default: false })
  public approvedWelcomeMessage!: boolean;

  @prop({ required: true, default: 0 })
  public reputation!: number;

  @prop({ required: true, default: Date.now })
  public lastAccessDate!: Date;

  @prop({ unique: true, trim: true })
  public auth?: string;

  // this will create a virtual property called 'fullName'
  public get fullName(this: DocumentType<User>) {
    return this.lastName ? `${this.firstName} ${this.lastName}` : this.firstName;
  }
  public set fullName(full) {
    let newFirstName: string = full;
    let newLastName: string = '';
    const split = full.split(' ');
    if (split.length > 1) {
      newFirstName = split[0];
      newLastName = split.slice(1, split.length).join(' ');
    }
    this.firstName = newFirstName || '';
    this.lastName = newLastName || '';
  }
}

const UserModel = getModelForClass(User);
export default UserModel;
