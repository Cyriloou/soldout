import { prop, modelOptions, getModelForClass, Ref } from '@typegoose/typegoose';
import { Schema } from 'mongoose';
import { VoteCategory, VoteType } from '../enums';
import { Sneaker } from './sneaker';
import { Picture } from './picture';
import { Question } from './question';
import { User } from './user';
import { Base } from './_base';

@modelOptions({
  schemaOptions: {
    collection: 'Votes',
  },
})
export class Vote extends Base {
  @prop({ required: true, enum: VoteType })
  public type!: VoteType;

  @prop({ required: true, enum: VoteCategory })
  public voteCategorie!: string;

  @prop({ refPath: 'which' })
  public kind?: Ref<Sneaker | Question | Picture>;

  @prop({ ref: 'User', refType: Schema.Types.ObjectId })
  public authorUID!: Ref<User>;
}

const VoteModel = getModelForClass(Vote);
export default VoteModel;
