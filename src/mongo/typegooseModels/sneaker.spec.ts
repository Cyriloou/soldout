import SneakerModel, { Sneaker } from './sneaker';
import { setupDB } from '../mongo-test-setup';
import mongoose from 'mongoose';
import _ from 'lodash';
import UserModel, { User } from './user';
import { SneakerGender } from '../enums';
import { userDataMini } from './user.spec';

export const sneakerDataMini: object = {
  gender: SneakerGender.male,
  brand: 'nike',
  modelName: 'air max',
  description: 'nike air max',
  fullDesc: 'nike air max',
  launchDate: new Date(),
};

// Setup a Test Database
setupDB();

describe('Test sneaker model', () => {
  it('Bdd empty', async () => {
    const count = await SneakerModel.find({}).estimatedDocumentCount();
    expect(count).toEqual(0);
  });

  describe('create sneaker', () => {
    it('create sneaker', async done => {
      const user = new UserModel(userDataMini as User);
      await user.save();
      const validSneaker = new SneakerModel({ ...sneakerDataMini, authorUID: user._id } as Sneaker);
      const savedSneaker = await validSneaker.save();
      expect(savedSneaker._id).toBeDefined();
      done();
    });

    it('create sneaker without required field should failed', async () => {
      const userWithoutRequiredField = new SneakerModel(_.omit(sneakerDataMini, ['brand', 'modelName']));
      let err;
      try {
        const savedSneakerWithoutRequiredField = await userWithoutRequiredField.save();
        err = savedSneakerWithoutRequiredField;
      } catch (error) {
        err = error;
      }
      expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
      expect(err.errors.brand).toBeDefined();
      expect(err.errors.modelName).toBeDefined();
    });
  });
});
