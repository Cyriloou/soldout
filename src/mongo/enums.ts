export enum Genders {
  MALE, // equal to "= 0"
  FEMALE, // equal to "= 1"
}

export enum Roles {
  Admin = 'admin',
  User = 'user',
  Guest = 'guest',
}

export enum Status {
  BeingProcessed = 'Being processed', // equal to "= 0"
  Approved = 'Approved', // equal to "= 1"
  Deny = 'Deny', // equal to "= 2"
}

export enum VoteCategory {
  Picture = 'Picture',
  Question = 'Question',
  Sneaker = 'Sneaker',
}

export enum VoteType {
  upvote = 'upvote',
  downvote = 'downvote',
  like = 'like',
}

export enum QuestionType {
  Question = 'Question',
  Answer = 'Answer',
}

export enum ReminderType {
  week = -7,
  day = -1,
}

export enum SentStatus {
  Pending = 'pending',
  Sent = 'sent',
  NotSent = 'notSent',
  EmailError = 'email error',
  RetryLater = 'retry later',
}

export enum ProductCategory {
  sneaker = 'SNEAKER',
}

export enum SneakerGender {
  male = 'male',
  female = 'female',
  kid = 'kid',
  unisex = 'unisex',
}

export enum RaffleType {
  raffle = 'raffle',
  sortieMagasin = 'sortie Magasin',
}
