// require mongoose module
import mongoose = require('mongoose');

interface IInput {
  dbURL: string;
}

const params = {
  autoIndex: false,
  connectTimeoutMS: 1000, // Give up initial connection after 1 seconds
  socketTimeoutMS: 3000, // Close sockets after 3 seconds of inactivity
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

export default ({ dbURL }: IInput) => {
  const connect = () => {
    mongoose.Promise = global.Promise;
    mongoose
      .connect(dbURL, params)
      // tslint:disable-next-line:no-console
      .then(() => console.info(`Successfully connected to ${dbURL}`))
      .catch(error => {
        // tslint:disable-next-line:no-console
        console.error('Error connecting to database: ', error);
        return process.exit(1);
      });
  };

  connect();

  const db = mongoose.connection;
  // tslint:disable-next-line:no-console
  db.once('open', () => console.log('Connexion à la base OK'));
  db.on('disconnected', connect);
  // tslint:disable-next-line:no-console
  db.on('connected', () => console.log('Connexion à la base OK'));
  // tslint:disable-next-line:no-console
  db.on('error', console.error.bind(console, 'connection error:'));
  db.on('reconnected', () => null);
};
