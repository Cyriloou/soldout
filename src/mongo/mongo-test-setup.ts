// test-setup.js
import mongoose, { Collection } from 'mongoose';
import '../utils/dotenv';

// mongoose.Promise = global.Promise;
// May require additional time for downloading MongoDB binaries
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

const mongooseOpts: mongoose.ConnectionOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}; // remove option 'useMongoClient' if you use mongoose 5 and above

async function removeAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    await collection.deleteMany({});
  }
}

async function dropAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    try {
      await collection.drop();
    } catch (error) {
      // This error happens when you try to drop a collection that's already dropped. Happens infrequently.
      // Safe to ignore.
      if (error.message === 'ns not found') {
        return;
      }
      // This error happens when you use it.todo.
      // Safe to ignore.
      if (error.message.includes('a background operation is currently running')) {
        return;
      }
      // tslint:disable-next-line:no-console
      console.log(error.message);
    }
  }
}

export function setupDB() {
  // Connect to Mongoose
  beforeAll(async () => {
    const mongoUri: string = process.env.STAG_DB_URI!;
    await mongoose.connect(mongoUri, mongooseOpts);
  });

  // Cleans up database between each test
  afterEach(async () => {
    await removeAllCollections();
  });

  // Disconnect Mongoose
  afterAll(async () => {
    return await dropAllCollections();
  });
}
