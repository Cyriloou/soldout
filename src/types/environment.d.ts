declare namespace NodeJS {
  export interface ProcessEnv {
    DB_URI: string;
    NODE_ENV: 'development' | 'production' | 'staging' | 'testing';
    PORT?: string;
    DB_NAME?: string;
  }
}
// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {};
