// requires
import _ from 'lodash';
import './dotenv';

/**
 * App Variables
 */

if (!process.env.PORT) {
  process.exit(1);
}

interface ISubConfig {
  app_desc?: string;
  app_name?: string;
  config_id?: string;
  dbURL?: string;
  json_indentation?: number;
  node_port?: number;
}

type TypeConfig = 'development' | 'production' | 'staging' | 'testing';
// module variables
const config: Record<TypeConfig, ISubConfig> = {
  development: {
    app_desc: 'my app desc',
    app_name: 'my app',
    config_id: 'development',
    dbURL: 'mongodb://localhost:27017/mongotest',
    json_indentation: 4,
    node_port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
  },
  production: {
    config_id: 'production',
    dbURL: process.env.PROD_DB_URI,
    node_port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
  },
  staging: {
    config_id: 'staging',
    dbURL: process.env.STAG_DB_URI,
    node_port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
  },
  testing: {
    config_id: 'testing',
  },
};

const defaultConfig: ISubConfig = config.development;
const environment: string = process.env.NODE_ENV || 'development';
// let myFruit: Fruit = myString as Fruit;
const environmentConfig: ISubConfig = config[environment as TypeConfig];
const finalConfig: ISubConfig = _.merge(defaultConfig, environmentConfig);

// as a best practice
// all global variables should be referenced via global. syntax
// and their names should always begin with g
global.gConfig = finalConfig;

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {};
