/**
 * Required External Modules
 */
import express, { Application, NextFunction } from 'express';
import * as bodyParser from 'body-parser';
import path from 'path';
import cors from 'cors';
// connect to mongoDB
import connect from './mongo/mongo';
import SneakersController from './controllers/sneakers.controller';
import UserController from './controllers/user.controller';
import errorMiddleware from './middleware/error.middleware';

class App {
  public app: Application;
  public port: number;
  // declaring our controller
  // public userController: UserController;
  // public postsController: SneakersController;

  constructor() {
    this.app = express();
    this.port = global.gConfig.node_port;

    this.setMongoConfig();
    this.initializeMiddlewares();
    this.initializeControllers();
    this.initializeErrorHandling();
    // Creating and assigning a new instance of our controller
  }

  public getServer() {
    return this.app;
  }

  public listen() {
    /**
     * Server Activation
     */
    this.app.listen(this.port, (err: any) => {
      if (err) {
        // tslint:disable-next-line:no-console
        return console.log(err);
      }
      // tslint:disable-next-line:no-console
      console.log(`App listening on the port ${this.port}`);
    });
  }

  // This serves everything in `static` as static files
  private initializeMiddlewares(): void {
    /**
     *  App Configuration
     */
    this.app.use(express.static(path.join(__dirname, '/../static/')));
    // Allows us to receive requests with data in json format
    this.app.use(bodyParser.json({ limit: '50mb' }));
    // Allows us to receive requests with data in x-www-form-urlencoded format
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    // Enables cors
    this.app.use(cors());
    this.app.use(this.loggerMiddleware);
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeControllers() {
    this.app.use('/posts', new SneakersController().router);
    this.app.use('/users', new UserController().router);
  }

  // Connecting to our MongoDB database
  private setMongoConfig() {
    const dbURL: string = global.gConfig.dbURL;
    connect({ dbURL });
  }

  private loggerMiddleware(request: express.Request, response: express.Response, next: NextFunction) {
    // tslint:disable-next-line:no-console
    console.log(`${request.method} ${request.path}`);
    next();
  }
}

export default App;
