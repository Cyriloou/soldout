import { Router } from 'express';
import { UserService } from '../services/user.service';

export class UserController {
  public router = Router();
  private userService: UserService;

  constructor() {
    this.userService = new UserService();
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get('/', this.userService.welcomeMessage);
    this.router.get('/test', this.userService.getAllUsers);
  }
}

export default UserController;
