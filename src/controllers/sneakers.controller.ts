import { Router, Request, Response } from 'express';
import SneakerModel, { Sneaker } from '../mongo/typegooseModels/sneaker';

class SneakerController {
  public path = '/';
  public router = Router();

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(this.path, this.welcomeMessage);
    this.router.post(this.path, this.createASneaker);
  }

  private welcomeMessage(req: Request, res: Response) {
    return res.status(200).send('Welcome to pokeAPI REST by Nya ^^');
  }

  private getAllsneakers = async (request: Request, response: Response) => {
    const sneakers = await SneakerModel.findById({}).exec();
    response.send(sneakers);
  };

  private createASneaker = (request: Request, response: Response) => {
    const post: Sneaker = request.body;
    // this.sneakers.push(post);
    response.send(post);
  };
}

export default SneakerController;
