# sold-out

## Installation


## Installer de base
sudo service mongod stop

## Importer le projet
*Attention, ne fonctionne pas si les autorisations n’ont pas été données.*
1. Dans Visual studio code, ouvrir le terminal (Ctrl+ù)
2. Se mettre dans le dossier de destination du repo via le terminal
3. Executer dans le terminal:
   ```
   git clone https://github.com/jeanmaximeguedel/sold-out
   ```
4. Telecharger les deux fichiers env dans le drive
5. Placer les fichier
```
mv "ficher env a placer dans le client client" client/.env
mv "env serveur a placer dans le root" .env
```

## Installer Node et React
1. Installer [NodeJs](https://nodejs.org/en/download/)
3. Executer dans le terminal:
   ```
   npm install --global npm
   npm install --global create-react-app
   npm install -g nodemon
   # executer a la racine du projet
   npm i
   cd client
   npm i
   ```
Attention:
- *npm n’est pas une commande de base sur windows*
- *Si on a Windows: Taper le chemin complet vers npm pour exécuter la commande dans le terminal*

## Importer la BDD
1. Aller chercher le fichier dump dans le drive // Fichier Installation)
2. Décompresser dans C:/dump
3. Ecrire dans l’invite de commande :
   ```
   cd c:\Program Files\MongoDB\Server\4.2\bin
   mongodump.exe --host Cluster0-shard-0/cluster0-shard-00-00-pu2m6.mongodb.net:27017,cluster0-shard-00-01-pu2m6.mongodb.net:27017,cluster0-shard-00-02-pu2m6.mongodb.net:27017 --ssl --username soldout_full  --password <ask JM for password> --authenticationDatabase admin --db soldout --out=C:/dump
   ```
   **/!\\** *le mot de passe n’est pas celui de mongodb, il est unique et seul jm le connait*
4. Lancer mondb
   ```
   cd "ProgramFiles\MongoDB\Server\4.2\bin"
   mongorestore --host=127.0.0.1 --port=27017 C:/dump
   ```

## Lancer la version dev
1. lancer dans le terminal (a la racine du projet)
   ```
   npm start
   cd client
   npm start
   ```
